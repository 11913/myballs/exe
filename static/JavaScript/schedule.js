window.onload = () => {
  const schedule = getEmployments();
  const toFind = document.querySelector(".sub");
  toFind.addEventListener("click", (e) => {
    const inputGroup = document.querySelector(".form-control.border-dark");
    const inputTeacher = document.querySelectorAll(
      ".form-control.border-dark"
    )[1];
    e.preventDefault();
    printSchedule(filtering(inputGroup.value, inputTeacher.value, schedule));
  });
};

const printEmptySchedule = () => {
  const columns = document.querySelectorAll(".schedule.col-md.mx-1.my-2.px-0");
  columns.forEach((col) => {
    for (let i = 0; i < col.children.length; i++) {
      if (col.children[i].tagName != "A") {
        col.children[i].innerHTML = "";
        if ((i - 1) % 12 == 0) col.children[i].classList = ["white-line"];
        else col.children[i].classList = [];
      }
    }
  });
};

const printSchedule = (schedule) => {
  const columns = document.querySelectorAll(".schedule.col-md.mx-1.my-2.px-0");
  const arrayDays = Object.keys(schedule[0]);
  let indexDay = 0;
  // Цикл для всех дней в рабочей неделе
  arrayDays.forEach((day) => {
    // columns[arrayDays.indexOf(day)] - column current day
    // schedule[0][day] - array emloyments
    schedule[0][day].forEach((empl) => {
      let teacher = empl["Преподаватель"];
      teacher = `${teacher.slice(0, teacher.indexOf(" "))} ${teacher.slice(
        teacher.indexOf(" "),
        teacher.indexOf(" ") + 2
      )}. ${teacher.slice(
        teacher.lastIndexOf(" "),
        teacher.lastIndexOf(" ") + 2
      )}. `;
      const date8_00 = Date.parse(day + "T08:00:00") / 60000 / 5;
      const dateStart = Date.parse(day + "T" + empl["Начало занятия"]);
      const dateFinish = Date.parse(day + "T" + empl["Окончания занятия"]);
      const breakStart = Date.parse(day + "T" + empl["Начало перерыва"]);
      const breakFinish = Date.parse(day + "T" + empl["Окончание перерыва"]);
      const indexStart = dateStart / 60000 / 5 - date8_00;
      const indexFinish = dateFinish / 60000 / 5 - date8_00;
      const indexBreakStart = breakStart / 60000 / 5 - date8_00;
      const indexBreakFinish = breakFinish / 60000 / 5 - date8_00;
      // (dateFinish - dateStart)/60000/5 - сколько нужно закрасить
      // columns[indexDay].children[index + 1] - элемент, который нужно изменить
      const scheduleDiv = columns[indexDay].children[indexStart + 1];
      scheduleDiv.classList = ["employment"];
      const emloymentDiv = document.createElement("div");
      emloymentDiv.classList.add("employment-text");
      scheduleDiv.appendChild(emloymentDiv);
      const startEmpl = empl["Начало занятия"];
      const endEmpl = empl["Окончания занятия"];
      const startBreak = empl["Начало перерыва"];
      const endBreak = empl["Окончание перерыва"];
      emloymentDiv.innerHTML = `
          <span class="employment__span">${empl["Дисциплина"]}</span><br/>
          <span class="employment__span">${empl["Группа"]}, ${
        empl["Аудитория"]
      }</span><br/>
          <span class="employment__span">${
            !startBreak
              ? `${startEmpl.slice(
                  0,
                  startEmpl.lastIndexOf(":")
                )}-${endEmpl.slice(0, endEmpl.lastIndexOf(":"))}`
              : `${startEmpl.slice(
                  0,
                  startEmpl.lastIndexOf(":")
                )}-${startBreak.slice(
                  0,
                  startBreak.lastIndexOf(":")
                )},${endBreak.slice(
                  0,
                  endBreak.lastIndexOf(":")
                )}-${endEmpl.slice(0, endEmpl.lastIndexOf(":"))}`
          }
            </span>
          <br/>
          <span>${teacher}</span>
        `;

      for (let i = indexStart; i < indexFinish; i++) {
        i !== indexFinish - 1
          ? (columns[indexDay].children[i + 2].classList = "invisible-div")
          : null;
        const newDiv = document.createElement("div");
        scheduleDiv.appendChild(newDiv);

        if (!(i >= indexBreakStart && i < indexBreakFinish))
          newDiv.classList.add("white-line");
        else {
          newDiv.classList.add("bg-yellow");
        }
        if (i === indexStart) {
          newDiv.classList.add("border-top");
          newDiv.classList.add("border-dark");
        }
        if (i === indexFinish - 1) {
          newDiv.classList.add("border-bottom");
          newDiv.classList.add("border-dark");
        }
      }
      scheduleDiv.style.setProperty(
        "--elements-count",
        scheduleDiv.children.length - 1
      );
    });
    indexDay++;
  });
};

const getEmployments = () => {
  const schedule = [];
  fetch("api/schedule")
    .then((response) => response.json())
    .then((json) => schedule.push(json))
    .then(() => printSchedule(schedule))
    .catch((error) => console.error(error));
  return schedule;
};

const filtering = (group, teacher, schedule) => {
  const filterArray = [{}];
  for (const dayName in schedule[0]) {
    const day = schedule[0][dayName];
    const arrayEmpls = day.filter((empl) => {
      if (group && teacher)
        return (
          empl.Группа.startsWith(group) &&
          empl.Преподаватель.startsWith(teacher)
        );
      if (group && !teacher) return empl.Группа.startsWith(group);
      if (!group && teacher) return empl.Преподаватель.startsWith(teacher);
      if (!group && !teacher) return empl;
    });
    filterArray[0][dayName] = arrayEmpls;
  }
  printEmptySchedule();
  return filterArray;
};

"""Import of something cool"""
from django.contrib import admin
from schedule import models

admin.site.register(models.Speciality)
admin.site.register(models.Group)
admin.site.register(models.Teacher)
admin.site.register(models.Audience)
admin.site.register(models.Lesson)
admin.site.register(models.Pause)
admin.site.register(models.PauseLesson)
admin.site.register(models.Discipline)
admin.site.register(models.Load)
admin.site.register(models.Employment)
admin.site.register(models.Shift)
admin.site.register(models.TypeEmployment)
admin.site.register(models.Employee)
admin.site.register(models.User)
admin.site.register(models.Subscribe)
admin.site.register(models.SubscribeGroup)
admin.site.register(models.SubscribeTeacher)

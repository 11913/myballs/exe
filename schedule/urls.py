"""Import of something cool"""
from django.urls import path
from . import views
from .views import ShiftAPIView, ScheduleAPIView

urlpatterns = [
    path('', views.schedule, name='schedule'),
    path('bells', views.bells, name='bells'),
    path('api/bells/shifts', ShiftAPIView.as_view()),
    path('api/schedule', ScheduleAPIView.as_view()),
    path('registration', views.registration, name='registration'),
]

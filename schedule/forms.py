"""Импорт Django"""
from django import forms
from django.contrib.auth.forms import UserCreationForm

from .models import User


class UserRegistrationForm (UserCreationForm):  # pylint: disable=too-few-public-methods
    """
        Содержит поля из формы регистрации пользователя

    """
    patronymic = forms.TextInput()
    password1 = forms.CharField(max_length=150,
                                widget=forms.PasswordInput(attrs={
                                    'placeholder': 'Пароль',
                                }))
    password2 = forms.CharField(max_length=150,
                                widget=forms.PasswordInput(attrs={
                                    'placeholder': 'Повторите пароль',
                                }))

    class Meta:  # pylint: disable=too-few-public-methods
        """
            Содержит букавки

        """
        model = User
        fields = ['username', 'last_name', 'first_name', 'patronymic',
                  'email', 'password1', 'password2']
        widgets = {
            'username': forms.TextInput(attrs={
                'placeholder': 'Логин', }),
            'last_name': forms.TextInput(attrs={
                'placeholder': 'Фамилия',
            }),
            'first_name': forms.TextInput(attrs={
                'placeholder': 'Имя',
            }),
            'patronymic': forms.TextInput(attrs={
                'placeholder': 'Отчество (необязательно)',
            }),
            'email': forms.TextInput(attrs={
                'type': 'email', 'placeholder': 'Эл.почта',
            }),
        }

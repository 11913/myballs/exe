"""Import of something cool"""
from django.apps import AppConfig


class ScheduleConfig(AppConfig):
    """
            Содержит букавки

    """
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'schedule'
    verbose_name = 'Расписание'

"""Import of something cool"""
from rest_framework import serializers
from schedule import models


class TeacherSerializer(serializers.ModelSerializer):  # pylint: disable=too-few-public-methods
    """
                Содержит букавки

    """
    class Meta:  # pylint: disable=too-few-public-methods
        """
                    Содержит букавки

        """
        model = models.Teacher
        fields = '__all__'


class PauseSerializer(serializers.ModelSerializer):  # pylint: disable=too-few-public-methods
    """
                Содержит букавки

    """
    class Meta:  # pylint: disable=too-few-public-methods
        """
                    Содержит букавки

        """
        model = models.Pause
        fields = ('start_time', 'end_time',)


class DisciplineSerializer(serializers.ModelSerializer):  # pylint: disable=too-few-public-methods
    """
                Содержит букавки

    """
    class Meta:  # pylint: disable=too-few-public-methods
        """
                    Содержит букавки

        """
        model = models.Discipline
        fields = ('abbreviation',)


class GroupSerializer(serializers.ModelSerializer):  # pylint: disable=too-few-public-methods
    """
                Содержит букавки

    """
    class Meta:  # pylint: disable=too-few-public-methods
        """
                    Содержит букавки

        """
        model = models.Group
        fields = '__all__'


class AudienceSerializer(serializers.ModelSerializer):  # pylint: disable=too-few-public-methods
    """
                Содержит букавки

    """
    class Meta:  # pylint: disable=too-few-public-methods
        """
                    Содержит букавки

        """
        model = models.Audience
        fields = ('title',)


class LessonsSerializer(serializers.ModelSerializer):  # pylint: disable=too-few-public-methods
    """
                Содержит букавки

    """
    class Meta:  # pylint: disable=too-few-public-methods
        """
            Содержит букавки

        """
        model = models.Lesson
        fields = ('start_time', 'end_time')


class PauseLessonSerializer(serializers.ModelSerializer):  # pylint: disable=too-few-public-methods
    """
                        Содержит букавки

    """
    class Meta:  # pylint: disable=too-few-public-methods
        """
                            Содержит букавки

        """
        model = models.PauseLesson
        fields = ('lesson', 'pause')


class EmploymentSerializer(serializers.ModelSerializer):  # pylint: disable=too-few-public-methods
    """
                        Содержит букавки

    """
    class Meta:  # pylint: disable=too-few-public-methods
        """
                            Содержит букавки

        """
        model = models.Employment
        fields = ('date',)


class ShiftSerializer(serializers.ModelSerializer):  # pylint: disable=too-few-public-methods
    """
                        Содержит букавки

    """
    class Meta:  # pylint: disable=too-few-public-methods
        """
                            Содержит букавки

        """
        model = models.Shift
        fields = "__all__"

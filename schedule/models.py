"""    Содержит букавки    """
import datetime
from django.db import models
from django.contrib.auth.models import AbstractUser

now = datetime.datetime.now()


class User(AbstractUser):  # pylint: disable=too-few-public-methods
    """
            Содержит букавки

    """
    last_name = models.CharField('Фамилия', max_length=150)
    first_name = models.CharField('Имя', max_length=150)
    patronymic = models.CharField('Отчество', max_length=150, blank=True)
    email = models.EmailField('Email', unique=True)

    def __str__(self):
        """
            Содержит букавки

        """
        return str(self.email)


class Speciality(models.Model):  # pylint: disable=too-few-public-methods
    """
                    Содержит букавки

    """
    title = models.TextField('Название')
    abbreviation = models.TextField('Сокращение')

    class Meta:  # pylint: disable=too-few-public-methods
        """
                        Содержит букавки

        """
        verbose_name = "Специальность"
        verbose_name_plural = "Специальности"

    def __str__(self):
        """
                        Содержит букавки

        """
        return str(self.abbreviation)


class Group(models.Model):  # pylint: disable=too-few-public-methods
    """
                    Содержит букавки

    """
    which_class = (
        ('9', 'После 9'),
        ('11', 'После 11')
    )

    cipher = models.SmallIntegerField('Шифр')
    year_of_admission = models.PositiveSmallIntegerField(
        'Год поступления', default=now.year)
    after_which_class = models.CharField(
        'После какого класса', choices=which_class, max_length=10)
    speciality = models.ForeignKey(
        Speciality, on_delete=models.CASCADE, verbose_name="Специальность")
    name = models.TextField("Название")

    class Meta:  # pylint: disable=too-few-public-methods
        """
                        Содержит букавки

        """
        verbose_name = "Группа"
        verbose_name_plural = "Группы"

    def __str__(self):
        """
                        Содержит букавки

        """
        return str(self.name)


class Teacher(models.Model):  # pylint: disable=too-few-public-methods
    """
                    Содержит букавки

    """
    user = models.ForeignKey(
        User, on_delete=models.CASCADE, verbose_name='Пользователь')

    class Meta:  # pylint: disable=too-few-public-methods
        """
                        Содержит букавки

        """
        verbose_name = "Преподаватель"
        verbose_name_plural = "Преподаватели"

    def __str__(self):
        """
                        Содержит букавки

        """
        return str(f"{self.user}")


class Employee(models.Model):  # pylint: disable=too-few-public-methods
    """
                    Содержит букавки

    """
    user = models.ForeignKey(
        User, on_delete=models.CASCADE, verbose_name='Пользователь')

    class Meta:  # pylint: disable=too-few-public-methods
        """
                        Содержит букавки

        """
        verbose_name = "Сотрудник уч. отдела"
        verbose_name_plural = "Сотрудники уч. отдела"

    def __str__(self):
        """
                        Содержит букавки

        """
        return str(f"{self.user}")


class Audience(models.Model):  # pylint: disable=too-few-public-methods
    """
                    Содержит букавки

    """
    title = models.TextField('Название')

    class Meta:  # pylint: disable=too-few-public-methods
        """
                        Содержит букавки

        """
        verbose_name = 'Аудитория'
        verbose_name_plural = 'Аудитории'

    def __str__(self):
        """
                        Содержит букавки

        """
        return str(self.title)


class Shift(models.Model):  # pylint: disable=too-few-public-methods
    """
                    Содержит букавки

    """
    title = models.PositiveSmallIntegerField('Название смены')
    num_first_lesson = models.PositiveSmallIntegerField('Номер первой пары')
    num_lust_lesson = models.PositiveSmallIntegerField('Номер последней пары')
    monday = models.BooleanField('Понедельник')
    tuesday = models.BooleanField('Вторник')
    wednesday = models.BooleanField('Среда')
    thursday = models.BooleanField('Четверг')
    friday = models.BooleanField('Пятница')
    saturday = models.BooleanField('Суббота')

    class Meta:  # pylint: disable=too-few-public-methods
        """
                        Содержит букавки

        """
        verbose_name = 'Смена'
        verbose_name_plural = 'Смены'

    def __str__(self):
        """
                        Содержит букавки

        """
        return str(f"Смена {self.title}")


class Lesson(models.Model):  # pylint: disable=too-few-public-methods
    """
                    Содержит букавки

    """
    shift_number = models.ForeignKey(
        Shift, on_delete=models.CASCADE, verbose_name="Номер смены")
    lesson_number = models.PositiveSmallIntegerField('Номер пары')
    start_time = models.TimeField('Время начала')
    end_time = models.TimeField('Время окончания')

    class Meta:  # pylint: disable=too-few-public-methods
        """
                        Содержит букавки

        """
        verbose_name = 'Пара'
        verbose_name_plural = 'Пары'

    def __str__(self):
        """
                        Содержит букавки

        """
        return str(f"Смена: {self.shift_number} Пара: {self.lesson_number}")


class Pause(models.Model):  # pylint: disable=too-few-public-methods
    """
                    Содержит букавки

    """
    start_time = models.TimeField('Время начала')
    end_time = models.TimeField('Время окончания')

    class Meta:  # pylint: disable=too-few-public-methods
        """
                        Содержит букавки

        """
        verbose_name = 'Перерыв'
        verbose_name_plural = 'Перерывы'

    def __str__(self):
        """
                        Содержит букавки

        """
        return str(f"{self.start_time}-{self.end_time}")


class PauseLesson(models.Model):  # pylint: disable=too-few-public-methods
    """
                    Содержит букавки

    """
    lesson = models.ForeignKey(
        Lesson, on_delete=models.CASCADE, verbose_name="Пара")
    pause = models.ForeignKey(
        Pause, on_delete=models.CASCADE, verbose_name="Перерыв", blank=True)

    class Meta:  # pylint: disable=too-few-public-methods
        """
                        Содержит букавки

        """
        verbose_name = 'Перерыв-Пара'
        verbose_name_plural = 'Перерывы-Пары'

    def __str__(self):
        """
                        Содержит букавки

        """
        return str(f"{self.lesson} {self.pause}")


class Discipline(models.Model):  # pylint: disable=too-few-public-methods
    """
                    Содержит букавки

    """
    title = models.TextField('Название')
    abbreviation = models.TextField('Сокращение')

    class Meta:  # pylint: disable=too-few-public-methods
        """
                        Содержит букавки

        """
        verbose_name = "Дисциплина"
        verbose_name_plural = "Дисциплины"

    def __str__(self):
        """
                        Содержит букавки

        """
        return str(self.abbreviation)


class TypeEmployment(models.Model):  # pylint: disable=too-few-public-methods
    """
                    Содержит букавки

    """
    title = models.TextField('Название')
    abbreviation = models.TextField('Сокращение')

    class Meta:  # pylint: disable=too-few-public-methods
        """
                        Содержит букавки

        """
        verbose_name = "Вид занятия"
        verbose_name_plural = "Виды занятий"

    def __str__(self):
        """
                        Содержит букавки

        """
        return str(self.abbreviation)


class Load(models.Model):  # pylint: disable=too-few-public-methods
    """
                    Содержит букавки

    """
    teacher = models.ForeignKey(
        Teacher, on_delete=models.CASCADE, verbose_name="Преподаватель")
    employee = models.ForeignKey(Employee,
                                 on_delete=models.CASCADE,
                                 verbose_name="Сотрудник уч. отдела")
    discipline = models.ForeignKey(
        Discipline, on_delete=models.CASCADE, verbose_name="Дисциплина")
    group = models.ForeignKey(
        Group, on_delete=models.CASCADE, verbose_name="Группа")
    type_employment = models.ForeignKey(
        TypeEmployment, on_delete=models.CASCADE, verbose_name="Вид занятия")
    total = models.PositiveSmallIntegerField('Всего')
    completed = models.PositiveSmallIntegerField('Проведено')
    semester = models.PositiveSmallIntegerField('Семестр')

    class Meta:  # pylint: disable=too-few-public-methods
        """
                        Содержит букавки

        """
        verbose_name = 'Нагрузка'
        verbose_name_plural = 'Нагрузки'

    def __str__(self):
        """
                        Содержит букавки

        """
        return str(f"{self.teacher}: {self.group} {self.discipline}")


class Employment(models.Model):  # pylint: disable=too-few-public-methods
    """
                    Содержит букавки

    """
    audience = models.ForeignKey(
        Audience, on_delete=models.CASCADE, verbose_name="Аудитория")
    lesson = models.ForeignKey(
        Lesson, on_delete=models.CASCADE, verbose_name="Пара")
    load = models.ForeignKey(
        Load, on_delete=models.CASCADE, verbose_name="Нагрузка")
    date = models.DateField('Дата')
    user = models.ForeignKey(
        User, on_delete=models.CASCADE, verbose_name="Пользователь")
    blocked = models.BooleanField('Заблокировано')

    class Meta:  # pylint: disable=too-few-public-methods
        """
                        Содержит букавки

        """
        verbose_name = 'Занятие'
        verbose_name_plural = 'Занятия'

    def __str__(self):
        """
                        Содержит букавки

        """
        return str(f"{self.date} {self.lesson} {self.load.teacher}\
{self.load.discipline} {self.audience}\
 ({self.load.group}) Заблокировано: {self.blocked}")


class Subscribe(models.Model):  # pylint: disable=too-few-public-methods
    """
                    Содержит букавки

    """
    user = models.ForeignKey(
        User, on_delete=models.CASCADE, verbose_name="Пользователь")

    class Meta:  # pylint: disable=too-few-public-methods
        """
                        Содержит букавки

        """
        verbose_name = 'Подписка'
        verbose_name_plural = 'Подписки'

    def __str__(self):
        """
                        Содержит букавки

        """
        return str(f'{self.user}')


class SubscribeGroup(models.Model):  # pylint: disable=too-few-public-methods
    """
                    Содержит букавки

    """
    subscribe = models.ForeignKey(
        Subscribe, on_delete=models.CASCADE, verbose_name="Подписка")
    group = models.ForeignKey(
        Group, on_delete=models.CASCADE, verbose_name="Группа")

    class Meta:  # pylint: disable=too-few-public-methods
        """
                        Содержит букавки

        """
        verbose_name = 'Подписка по группе'
        verbose_name_plural = 'Подписки по группам'

    def __str__(self):
        """
                        Содержит букавки

        """
        return str(f'{self.subscribe} подписан по группе {self.group}')


class SubscribeTeacher(models.Model):  # pylint: disable=too-few-public-methods
    """
                    Содержит букавки

    """
    subscribe = models.ForeignKey(
        Subscribe, on_delete=models.CASCADE, verbose_name="Подписка")
    teacher = models.ForeignKey(
        Teacher, on_delete=models.CASCADE, verbose_name="Преподаватель")

    class Meta:  # pylint: disable=too-few-public-methods
        """
                        Содержит букавки

        """
        verbose_name = 'Подписка по преподавателю'
        verbose_name_plural = 'Подписки по преподавателям'

    def __str__(self):
        """
                        Содержит букавки

        """
        return str(f'{self.subscribe} подписан по преподавателю {self.teacher}')

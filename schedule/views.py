"""Import of something cool"""
from datetime import datetime, timedelta

from django.shortcuts import render
from django.http import HttpResponseRedirect

from rest_framework.views import APIView
from rest_framework.response import Response

from schedule import models
from schedule.serializers import ShiftSerializer
from schedule.forms import UserRegistrationForm


def schedule(request):
    """
                Содержит букавки

    """
    return render(request, 'schedule.html', {
        'cell': range(10),
        'column': range(10),
        'day': [((datetime.now() + timedelta(days=i)).day,
                 (datetime.now() + timedelta(days=i)).strftime('%a'))
                for i in range(8)
                if (datetime.now() + timedelta(days=i)).weekday() != 6],
        'hour': range(8, 19),

    })


class ScheduleAPIView(APIView):
    """
                Содержит букавки

    """

    def get(self, request):
        """
                    Содержит букавки

        """

        result = {}  # Словарь, который будем возвращать в результате запроса
        current_date = datetime.now().date() - timedelta(days=1)

        while len(result) < 7:
            current_date = current_date + timedelta(days=1)
            if current_date.isoweekday() == 7:
                continue

            # Массив занятий для дня недели
            date_list = []

            # Добавляем к текущей дате список занятий
            result[str(current_date)] = date_list

            # Словарь по которому будет проводиться фильтрация занятий
            qwargs_by_employments = {}

            if 'group' in request.GET:
                group = models.Group.objects.filter(
                    id=int(request.GET['group']))
                if group.count() == 1:
                    qwargs_by_employments['load__group'] = group.get()

            if 'teacher' in request.GET:
                teacher = models.Teacher.objects.filter(
                    id=request.GET['teacher'])
                if teacher.count() == 1:
                    qwargs_by_employments['load__teacher'] = teacher.get()

            employments = models.Employment.objects.filter(
                date=current_date, **qwargs_by_employments)

            for employment in employments:

                lesson_data = {
                    "Дисциплина": employment.load.discipline.title,
                    'Начало занятия': employment.lesson.start_time,
                    'Окончания занятия': employment.lesson.end_time,
                    'Преподаватель':
                        f"{employment.load.teacher.user.last_name} "
                        f"{employment.load.teacher.user.first_name} "
                        f"{employment.load.teacher.user.patronymic}",
                    'Группа': employment.load.group.name,
                    'Аудитория': employment.audience.title,
                }
                pause_lesson = models.PauseLesson.objects.filter(
                    lesson=employment.lesson)
                if pause_lesson.count() > 0:
                    pause_lesson = pause_lesson.get()
                    lesson_data['Начало перерыва'] = \
                        pause_lesson.pause.start_time
                    lesson_data['Окончание перерыва'] = \
                        pause_lesson.pause.end_time
                date_list.append(lesson_data)

        return Response(result)


def bells(request):
    """
                Содержит букавки

    """
    return render(request, "bells.html")


class ShiftAPIView(APIView):
    """
                Содержит букавки

    """
    def get(self):
        """
                    Содержит букавки

        """
        shifts = models.Shift.objects.all()

        return Response(ShiftSerializer(shifts, many=True).data)


def registration(request):
    """
                Содержит букавки

    """
    if request.method == 'POST':
        form_registration = UserRegistrationForm(request.POST)
        if form_registration.is_valid():
            form_registration.save()
            return HttpResponseRedirect("accounts/login")
    else:
        form_registration = UserRegistrationForm()
    return render(request, "registration.html", {
        'form_registration': form_registration
    })
